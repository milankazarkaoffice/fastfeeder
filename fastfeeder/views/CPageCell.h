//
//  CPageCell.h
//  fastfeeder
//
//  Created by Milan Kazarka on 8/9/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CPageCellDelegate <NSObject>
-(void)onPageCellPlay:(id)cell;
-(void)onPageOpenURL:(id)cell;
@end

@interface CPageCell : UITableViewCell {
@public
    IBOutlet UILabel *mtitle;
    IBOutlet UILabel *msumary;
    IBOutlet UILabel *mpaginginfo;
    NSString *murl; // website of the item
    
    NSObject<CPageCellDelegate> *delegate;
}

-(IBAction)onPlay:(id)sender;
-(IBAction)onOpenURL:(id)sender;

@end
