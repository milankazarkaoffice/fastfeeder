//
//  CSquare.m
//  vysokoStreamer
//
//  Created by Milan Kazarka on 9/19/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import "CSquare.h"

@implementation CSquare

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    cr = 100;
    cg = 120;
    cb = 100;
    ca = _A_BEGIN;
    ntrend = 1;
    
    return self;
}

-(void)updateColor
{
    if (ca==_A_END)
    {
        ntrend = -1;
    }
    else if (ca==_A_BEGIN)
    {
        ntrend = 1;
    }
    ca += ntrend;
    
    [self setBackgroundColor:[UIColor colorWithRed:(float)cr/255.0f
                                             green:(float)cg/255.0f
                                              blue:(float)cb/255.0f
                                             alpha:(float)ca/255.0f]];
}

@end
