//
//  CSquare.h
//  vysokoStreamer
//
//  Created by Milan Kazarka on 9/19/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>

#define _A_BEGIN 10
#define _A_END 190

@interface CSquare : UIView {
@public
    unsigned char cr;
    unsigned char cg;
    unsigned char cb;
    unsigned char ca;
    int ntrend;
}

-(void)updateColor;

@end
