//
//  CItemCell.h
//  fastfeeder
//
//  Created by Milan Kazarka on 8/9/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWFeedItem.h"

@interface CItemCell : UITableViewCell {
@public
    IBOutlet UIView *mcontent;
    IBOutlet UIImageView *mbackground;
    IBOutlet UILabel *mtitle;
    IBOutlet UILabel *mdate;
}

-(void)loadFeedItem:(MWFeedItem*)item;
-(void)loadImageURL:(NSString*)strurl;

@end
