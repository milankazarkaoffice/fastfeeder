//
//  CFeedCell.m
//  fastfeeder
//
//  Created by Milan Kazarka on 8/10/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import "CFeedCell.h"

@implementation CFeedCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

-(IBAction)onActive:(id)sender
{
    if (delegate)
        [delegate onFeedLoad:self];
}

-(IBAction)onURL:(id)sender
{
    if (delegate)
        [delegate onFeedURL:self];
}

@end
