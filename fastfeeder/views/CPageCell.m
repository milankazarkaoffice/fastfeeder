//
//  CPageCell.m
//  fastfeeder
//
//  Created by Milan Kazarka on 8/9/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import "CPageCell.h"

@implementation CPageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
   
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

-(IBAction)onPlay:(id)sender
{
    if (delegate)
    {
        [delegate onPageCellPlay:self];
    }
}

-(IBAction)onOpenURL:(id)sender
{
    if (delegate)
    {
        [delegate onPageOpenURL:self];
    }
}

@end
