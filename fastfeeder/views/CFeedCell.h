//
//  CFeedCell.h
//  fastfeeder
//
//  Created by Milan Kazarka on 8/10/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CFeedSource.h"

@protocol CFeedCellDelegate <NSObject>
-(void)onFeedLoad:(id)feed;
-(void)onFeedURL:(id)feed;
@end

@interface CFeedCell : UITableViewCell {
@public
    IBOutlet UILabel *mtitle;
    IBOutlet UILabel *mrssurl; // rss URL
    IBOutlet UILabel *murl; // official website
    IBOutlet UIButton *mactive; // active area button
    IBOutlet UILabel *mpaginginfo;
    
    CFeedSource *msource;
    
    NSObject<CFeedCellDelegate> *delegate;
}

-(IBAction)onActive:(id)sender;
-(IBAction)onURL:(id)sender;

@end
