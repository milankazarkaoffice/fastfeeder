//
//  CItemCell.m
//  fastfeeder
//
//  Created by Milan Kazarka on 8/9/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import "CItemCell.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>

@implementation CItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

-(void)loadImageURL:(NSString*)strurl
{
    [mbackground setImageWithURL: [NSURL URLWithString:strurl]];
}

-(void)loadFeedItem:(MWFeedItem*)item
{
    NSLog(@"CItemCell loadFeedItem");
    
    CALayer * l = [mcontent layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:10.0];
    
    if (item.content)
    {
        NSLog(@"CItemCell loadFeedItem content (\n%@\n)",item.content);
    }
    if (item.link)
    {
        NSLog(@"CItemCell loadFeedItem link (\n%@\n)",item.link);
    }
    
    if (item.enclosures)
    {
        if ([item.enclosures count]>0)
        {
            int n;
            for(n = 0; n < [item.enclosures count]; n++)
            {
                NSDictionary *endict = [item.enclosures objectAtIndex:n];
                NSLog(@"enclosure type %@ url %@",[endict objectForKey:@"type"],[endict objectForKey:@"url"]);
            }
        }
    }
    else
    {
        NSLog(@"no enclosures");
    }
    
    //[mbackground setImageWithURL:[NSURL URLWithString:@"http://blog.newyorker.com/online/blogs/newsdesk/173139448-HP.jpg"]];
}

@end
