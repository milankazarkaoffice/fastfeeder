//
//  CT2S.h
//  fastfeeder
//
//  Created by Milan Kazarka on 8/9/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FliteTTS;

@interface CT2S : NSObject {
@public
    FliteTTS *fliteEngine;
}

-(id)init;
+(id)instance;

@end
