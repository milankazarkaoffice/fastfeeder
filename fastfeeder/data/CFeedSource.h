//
//  CFeedSource.h
//  fastfeeder
//
//  Created by Milan Kazarka on 8/10/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CFeedSource : NSObject {
@public
    NSString *mstrURL;
    NSString *mstrName;
    NSString *mstrExt; // official URL of the provider of the feed
}

-(id)initWithURL:(NSString*)strURL name:(NSString*)strName external:(NSString*)strExt;

@end
