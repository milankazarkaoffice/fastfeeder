//
//  CFeedSource.m
//  fastfeeder
//
//  Created by Milan Kazarka on 8/10/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import "CFeedSource.h"

@implementation CFeedSource

-(id)initWithURL:(NSString*)strURL name:(NSString*)strName external:(NSString*)strExt
{
    self = [super init];
    
    mstrURL = [[NSString alloc] initWithString:strURL];
    mstrName = [[NSString alloc] initWithString:strName];
    mstrExt = [[NSString alloc] initWithString:strExt];
    
    return self;
}

@end
