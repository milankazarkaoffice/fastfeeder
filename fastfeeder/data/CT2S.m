//
//  CT2S.m
//  fastfeeder
//
//  Created by Milan Kazarka on 8/9/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import "CT2S.h"
#import "FliteTTS.h"

static CT2S *CT2SInstance = nil;

@implementation CT2S

+(id)instance
{
    if (!CT2SInstance)
        CT2SInstance = [[CT2S alloc] init];
    return CT2SInstance;
}

-(id)init
{
    if (CT2SInstance)
        return CT2SInstance;
    self = [super init];
    CT2SInstance = self;
    
    fliteEngine = [[FliteTTS alloc] init];
    [fliteEngine setPitch:90.0 variance:45.0 speed:1.0];	// Change the voice properties
	
    /**
     if([voicename isEqualToString:@"cmu_us_kal"]) {
     voice = register_cmu_us_kal();
     }
     else if([voicename isEqualToString:@"cmu_us_kal16"]) {
     voice = register_cmu_us_kal16();
     }
     else if([voicename isEqualToString:@"cmu_us_rms"]) {
     voice = register_cmu_us_rms();
     }
     else if([voicename isEqualToString:@"cmu_us_awb"]) {
     voice = register_cmu_us_awb();
     }
     else if([voicename isEqualToString:@"cmu_us_slt"]) {
     voice = register_cmu_us_slt();
     }
     */
    [fliteEngine setVoice:@"cmu_us_rms"]; // rms
    
    return self;
}

@end
