//
//  CAboutViewController.h
//  fastfeeder
//
//  Created by Milan Kazarka on 9/30/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CAboutViewController : UIViewController {
    
}

-(IBAction)onDone:(id)sender;

@end
