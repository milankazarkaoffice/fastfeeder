//
//  CMainViewController.m
//  fastfeeder
//
//  Created by Milan Kazarka on 8/9/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import "CMainViewController.h"
#import "CFeedViewController.h"
#import "CPageViewController.h"
#import "CFeedSource.h"
#import "CT2S.h"
#import "FliteTTS.h"
#import "CAboutViewController.h"
#import "CCommon.h"

#define _SQ_WIDTH 30.0f
#define _SQ_HEIGHT 30.0f
#define _CLOCKTIK 40000

@interface CMainViewController ()

@end

@implementation CMainViewController

+(UITableViewCell*) createNewCustomCellFromNibNamed:(NSString*)name {
    NSArray* nibContents = [[NSBundle mainBundle]
                            loadNibNamed:name owner:self options:NULL];
    NSEnumerator *nibEnumerator = [nibContents objectEnumerator];
    UITableViewCell *customCell= nil;
    NSObject* nibItem = nil;
    while ( (nibItem = [nibEnumerator nextObject]) != nil) {
        if ( [nibItem isKindOfClass: (NSClassFromString(name))]) {
            customCell = (UITableViewCell*) nibItem;
            if ([customCell.reuseIdentifier isEqualToString: name]) {
                break; // we have a winner
            }
            
        }
    }
    return customCell;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    NSLog(@"initWithNibName");
    nrun = 0;
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    NSLog(@"initWithCoder");
    nrun = 0;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    msquares = [[NSMutableArray alloc] init];
    
    int x;
    int y;
    for(y = 0; y < self.view.bounds.size.height/_SQ_HEIGHT; y++)
    {
        for(x = 0; x < self.view.bounds.size.width/_SQ_WIDTH; x++)
        {
            CSquare *square = [[CSquare alloc] initWithFrame:CGRectMake(x*_SQ_WIDTH,y*_SQ_HEIGHT,_SQ_WIDTH,_SQ_HEIGHT)];
            
            int seed = x-y;
            if ( seed < _A_BEGIN )
                seed = _A_BEGIN;
            else if ( seed > _A_END )
                seed = _A_END;
            
            square->ca = seed;
            
            square->cr += x*5;
            square->cb += y*5;
            
            [msquares addObject:square];
            [mbackground addSubview:square];
        }
    }
    [NSThread detachNewThreadSelector:@selector(updateSquaresLoopThread) toTarget:self withObject:nil];
    
    mscroll.delegate = self;
    msources = [[NSMutableArray alloc] init];
    
    // bbc UK - http://feeds.bbci.co.uk/news/uk/rss.xml
    // bbc top - http://feeds.bbci.co.uk/news/rss.xml
    CFeedSource *bbcsource = [[CFeedSource alloc] initWithURL:@"http://feeds.bbci.co.uk/news/rss.xml" name:@"BBC news feed" external:@"http://www.bbc.co.uk"];
    CFeedSource *dmsource = [[CFeedSource alloc] initWithURL:@"http://www.dailymail.co.uk/home/index.rss" name:@"Daily Mail" external:@"http://www.dailymail.co.uk"];
    CFeedSource *cnetsource = [[CFeedSource alloc] initWithURL:@"http://www.cnet.co.uk/rss/editorial/n-1z0zzcw/" name:@"CNET" external:@"http://www.cnet.co.uk"];
    CFeedSource *tgsource = [[CFeedSource alloc] initWithURL:@"http://feeds.theguardian.com/theguardian/uk-news/rss" name:@"The Guardian" external:@"http://www.theguardian.com"];
    CFeedSource *reutersuksource = [[CFeedSource alloc] initWithURL:@"http://mf.feeds.reuters.com/reuters/UKdomesticNews" name:@"Reuters UK news" external:@"http://www.reuters.com"];
    CFeedSource *cnnsource = [[CFeedSource alloc] initWithURL:@"http://rss.cnn.com/rss/edition.rss" name:@"CNN news feed" external:@"http://www.cnn.com"];
    CFeedSource *nytsource = [[CFeedSource alloc] initWithURL:@"http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml" name:@"New York Times feed" external:@"http://nytimes.com"];
    CFeedSource *upisource = [[CFeedSource alloc] initWithURL:@"http://rss.upi.com/news/news.rss" name:@"U P I" external:@"http://www.upi.com/"];
    CFeedSource *reuterssource = [[CFeedSource alloc] initWithURL:@"http://feeds.reuters.com/Reuters/worldNews" name:@"Reuters" external:@"http://www.reuters.com"];
    CFeedSource *skysource = [[CFeedSource alloc] initWithURL:@"http://news.sky.com/feeds/rss/world.xml" name:@"Sky News" external:@"http://www.sky.com"];
    CFeedSource *nysource = [[CFeedSource alloc] initWithURL:@"http://www.newyorker.com/services/mrss/feeds/everything.xml" name:@"New Yorker" external:@"http://www.newyorker.com"];
    
#ifdef _VERSION_UK
    // http://feeds.theguardian.com/theguardian/uk-news/rss
    // http://www.dailymail.co.uk/home/index.rss
    // http://mf.feeds.reuters.com/reuters/UKdomesticNews
    // http://www.cnet.co.uk/rss/editorial/n-1z0zzcw/
    [msources addObject:reutersuksource];
    [msources addObject:bbcsource];
    [msources addObject:dmsource];
    [msources addObject:cnetsource];
#endif
#ifdef _VERSION_US
    [msources addObject:cnnsource];
    [msources addObject:reuterssource];
    [msources addObject:nytsource];
    [msources addObject:skysource];
    [msources addObject:nysource];
    [msources addObject:upisource];
    [msources addObject:bbcsource];
#endif
    
    [mscroll setContentSize:CGSizeMake(mscroll.frame.size.width*[msources count],mscroll.frame.size.height)];
    int n;
    for(n = 0; n < [msources count]; n++)
    {
        CFeedSource *source = [msources objectAtIndex:n];
        CFeedCell *mpage = (CFeedCell*)[CMainViewController createNewCustomCellFromNibNamed:@"CFeedCell"];
        mpage.selectionStyle = UITableViewCellSelectionStyleNone;
        [mpage setFrame:CGRectMake(n*mscroll.frame.size.width,0.0f,mscroll.frame.size.width,mscroll.frame.size.height)];
        mpage->delegate = self;
        [mpage->mtitle setText:source->mstrName];
        mpage->msource = source;
        [mpage->mpaginginfo setText:[NSString stringWithFormat:@"%d/%d",(n+1),[msources count]]];
        
        [mscroll addSubview:mpage];
    }

}

-(void)updateSquares
{
    int n;
    CSquare *square = nil;
    for(n = 0; n < [msquares count]; n++)
    {
        square = [msquares objectAtIndex:n];
        [square updateColor];
    }
}

-(void)updateSquaresThread
{
    [self performSelectorOnMainThread: @selector(updateSquares) withObject:nil waitUntilDone: YES];
}

-(void)updateSquaresLoopThread
{
    while(1)
    {
        [self updateSquaresThread];
        usleep(_CLOCKTIK);
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    if (nrun==0)
    {
        NSString *tell = @"Welcome to Fast Feeder. To select the news feed swipe the screen right or left. To load the news items in the feed tap on the screen. You are at the Reuters news feed.";
    
        CT2S *t2s = [CT2S instance];
        [t2s->fliteEngine stopTalking];
        [t2s->fliteEngine speakText:tell];
    }
    else
    {
        int n;
        for(n = 0; n < [msources count]; n++)
        {
            if (
                mscroll.contentOffset.x==(mscroll.frame.size.width*n)
                )
            {
                CFeedSource *source = [msources objectAtIndex:n];
                CT2S *t2s = [CT2S instance];
                [t2s->fliteEngine stopTalking];
                [t2s->fliteEngine speakText:source->mstrName];
                
                break;
            }
        }
    }
    nrun++;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(IBAction)onTestFeed:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    if (storyboard)
    {
        CPageViewController *cc = [storyboard instantiateViewControllerWithIdentifier:@"CPageViewController"];
        if (cc)
        {
            [self.navigationController pushViewController:cc animated:YES];
            [cc loadURL:@"http://feeds.reuters.com/Reuters/worldNews"];
        }
        
        /**
        CFeedViewController *cc = [storyboard instantiateViewControllerWithIdentifier:@"CFeedViewController"];
        if (cc)
        {
            [self.navigationController pushViewController:cc animated:YES];
            // http://www.newyorker.com/services/mrss/feeds/everything.xml
            // http://rss.nytimes.com/services/xml/rss/nyt/World.xml
            [cc loadFeedURL:@"http://www.newyorker.com/services/mrss/feeds/everything.xml"];
        }
        */
    }
}

-(void)onFeedLoad:(id)feed
{
    CFeedCell *cell = feed;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    if (storyboard)
    {
        CPageViewController *cc = [storyboard instantiateViewControllerWithIdentifier:@"CPageViewController"];
        if (cc)
        {
            [self.navigationController pushViewController:cc animated:YES];
            [cc loadURL:cell->msource->mstrURL];
        }
    }
}

-(void)onFeedURL:(id)feed
{
    CFeedCell *cell = feed;
    NSURL *url = [NSURL URLWithString:cell->msource->mstrExt];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewDidScroll");
    
    int n;
    for(n = 0; n < [msources count]; n++)
    {
        if (
            scrollView.contentOffset.x==(mscroll.frame.size.width*n)
            )
        {
            CFeedSource *source = [msources objectAtIndex:n];
            CT2S *t2s = [CT2S instance];
            [t2s->fliteEngine stopTalking];
            [t2s->fliteEngine speakText:source->mstrName];
            
            break;
        }
    }
}

-(IBAction)onAbout:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    if (storyboard)
    {
        CAboutViewController *cc = [storyboard instantiateViewControllerWithIdentifier:@"CAboutViewController"];
        if (cc)
        {
            [self presentViewController:cc animated:YES completion:nil];
        }
    }
}

@end
