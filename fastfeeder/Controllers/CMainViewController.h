//
//  CMainViewController.h
//  fastfeeder
//
//  Created by Milan Kazarka on 8/9/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CFeedSource.h"
#import "CFeedCell.h"
#import "CSquare.h"

@interface CMainViewController : UIViewController <UIScrollViewDelegate,CFeedCellDelegate> {
@public
    int nrun;
    NSMutableArray *msources; // feed sources
    
    IBOutlet UIView *mbackground;
    IBOutlet UIScrollView *mscroll;
    NSMutableArray *msquares;
}

-(IBAction)onTestFeed:(id)sender;
-(IBAction)onAbout:(id)sender;

@end
