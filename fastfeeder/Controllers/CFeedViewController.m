//
//  CFeedViewController.m
//  fastfeeder
//
//  Created by Milan Kazarka on 8/9/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import "CFeedViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NSString+HTML.h"
#import "CT2S.h"
#import "FliteTTS.h"
#import "CCommon.h"
#import "CItemCell.h"

#define _CELL_HEIGHT 120.0f

@interface CFeedViewController ()

@end

@implementation CFeedViewController

@synthesize itemsToDisplay;

+(UITableViewCell*) createNewCustomCellFromNibNamed:(NSString*)name {
    NSArray* nibContents = [[NSBundle mainBundle]
                            loadNibNamed:name owner:self options:NULL];
    NSEnumerator *nibEnumerator = [nibContents objectEnumerator];
    UITableViewCell *customCell= nil;
    NSObject* nibItem = nil;
    while ( (nibItem = [nibEnumerator nextObject]) != nil) {
        if ( [nibItem isKindOfClass: (NSClassFromString(name))]) {
            customCell = (UITableViewCell*) nibItem;
            if ([customCell.reuseIdentifier isEqualToString: name]) {
                break; // we have a winner
            }
            
        }
    }
    return customCell;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"CFeedViewController viewDidLoad");
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    gesture.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:gesture];
    
    [super viewDidLoad];
    mplayControlPanel.layer.cornerRadius = 10.0f;
    indexplaying = -1;
    
    // Setup
	//self.title = @"Loading...";
	formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterShortStyle];
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	parsedItems = [[NSMutableArray alloc] init];
	self.itemsToDisplay = [NSArray array];
	
	/**
	// Parse
	NSURL *feedURL = [NSURL URLWithString:@"http://rss.nytimes.com/services/xml/rss/nyt/Europe.xml"];
	feedParser = [[MWFeedParser alloc] initWithFeedURL:feedURL];
	feedParser.delegate = self;
	feedParser.feedParseType = ParseTypeFull; // Parse feed info and all items
	feedParser.connectionType = ConnectionTypeAsynchronously;
	[feedParser parse];
     */
}

-(void)viewWillDisappear:(BOOL)animated
{
    CT2S *t2s = [CT2S instance];
    [t2s->fliteEngine stopTalking];
}

-(void)loadFeedURL:(NSString*)strurl
{
    NSURL *feedURL = [NSURL URLWithString:strurl];
	feedParser = [[MWFeedParser alloc] initWithFeedURL:feedURL];
	feedParser.delegate = self;
	feedParser.feedParseType = ParseTypeFull; // Parse feed info and all items
	feedParser.connectionType = ConnectionTypeAsynchronously;
	[feedParser parse];
}

- (void)updateTableWithParsedItems {
	self.itemsToDisplay = parsedItems;
	mtableView.userInteractionEnabled = YES;
	mtableView.alpha = 1;
	[mtableView reloadData];
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedItem:(MWFeedItem *)item {
	NSLog(@"Parsed Feed Item: “%@”", item.title);
	if (item) [parsedItems addObject:item];
}

- (void)feedParserDidFinish:(MWFeedParser *)parser {
	NSLog(@"Finished Parsing%@", (parser.stopped ? @" (Stopped)" : @""));
    [self updateTableWithParsedItems];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(IBAction)onNavigationBack:(id)sender
{
    CT2S *t2s = [CT2S instance];
    [t2s->fliteEngine stopTalking];
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)onRefresh:(id)sender
{
    NSLog(@"onRefresh");
    [feedParser parse];
}

// tableview delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"CFeedViewController numberOfRowsInSection %d",itemsToDisplay.count);
    return itemsToDisplay.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    /**
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    */
    
    static NSString *CellIdentifier = @"CItemCell";
    
    CItemCell *cell = nil;
    
    cell = (CItemCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
    	cell = (CItemCell*)[CFeedViewController createNewCustomCellFromNibNamed:@"CItemCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if (indexplaying==indexPath.row)
    {
        //[cell->mbackground setImage:[UIImage imageNamed:@"saveButton@2x.png"]];
    }
    
	// Configure the cell.
	MWFeedItem *item = [itemsToDisplay objectAtIndex:indexPath.row];
	if (item) {
		
		// Process
		NSString *itemTitle = item.title ? [item.title stringByConvertingHTMLToPlainText] : @"[No Title]";
		NSString *itemSummary = item.summary ? [item.summary stringByConvertingHTMLToPlainText] : @"[No Summary]";
		
        [cell->mtitle setText:itemTitle];
        if (item.date) [ cell->mdate setText:[NSString stringWithFormat:@"%@", [formatter stringFromDate:item.date]]];
        
        [cell loadFeedItem:item];
        
        /**
		// Set
		cell.textLabel.font = [UIFont boldSystemFontOfSize:15];
		cell.textLabel.text = itemTitle;
		NSMutableString *subtitle = [NSMutableString string];
		if (item.date) [subtitle appendFormat:@"%@: ", [formatter stringFromDate:item.date]];
		[subtitle appendString:itemSummary];
		cell.detailTextLabel.text = subtitle;
		*/
	}
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CT2S *t2s = [CT2S instance];
    //[t2s->fliteEngine stopTalking];
    
    MWFeedItem *item = (MWFeedItem *)[itemsToDisplay objectAtIndex:indexPath.row];
    
    NSString *tell = @"";
    if (item.summary)
    {
        tell = [[NSString alloc] initWithFormat:@"%@",[item.summary stringByConvertingHTMLToPlainText]];
        //tell = [[NSString alloc] initWithFormat:@"%@ ... ... ... ... ... ... ... ... %@",[item.title stringByConvertingHTMLToPlainText],[item.summary stringByConvertingHTMLToPlainText]];
    }
    else
    {
        tell = [[NSString alloc] initWithFormat:@"%@",[item.title stringByConvertingHTMLToPlainText]];
    }
    
    tell = [tell stringByReplacingOccurrencesOfString:@"|" withString:@""];
    tell = [tell stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    tell = [tell stringByReplacingOccurrencesOfString:@";" withString:@""];
    tell = [tell stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    
    NSLog(@"play (\n%@\n)",tell);
    [t2s->fliteEngine speakText:[item.title stringByConvertingHTMLToPlainText]];
    if ([item.summary length]>0)
        [t2s->fliteEngine speakText:tell];
    indexplaying = indexPath.row;
    //[mtableView reloadData];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return _CELL_HEIGHT;
}

@end
