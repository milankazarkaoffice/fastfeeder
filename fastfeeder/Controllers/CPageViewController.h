//
//  CPageViewController.h
//  fastfeeder
//
//  Created by Milan Kazarka on 8/9/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWFeedParser.h"
#import "CPageCell.h"
#import "MBProgressHUD.h"

@interface CPageViewController : UIViewController <MWFeedParserDelegate,CPageCellDelegate,UIScrollViewDelegate> {
    MBProgressHUD *HUD;
@public
    IBOutlet UIScrollView *mscrollView;
    IBOutlet UILabel *mfeedtitle;
    
    MWFeedParser *feedParser;
	NSMutableArray *parsedItems;
    NSMutableArray *mpages; // pages
}

-(IBAction)onNavigationBack:(id)sender;
-(void)loadURL:(NSString*)strurl;

@property (nonatomic, retain) NSArray *itemsToDisplay;

@end
