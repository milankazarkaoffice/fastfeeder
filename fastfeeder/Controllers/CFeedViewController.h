//
//  CFeedViewController.h
//  fastfeeder
//
//  Created by Milan Kazarka on 8/9/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWFeedParser.h"

@interface CFeedViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,MWFeedParserDelegate> {
@public
    IBOutlet UIView *mplayControlPanel;
    IBOutlet UITableView *mtableView;
    
    // Parsing
	MWFeedParser *feedParser;
	NSMutableArray *parsedItems;
	
	// Displaying
	NSArray *itemsToDisplay;
	NSDateFormatter *formatter;
    
    int indexplaying; // which index are we playing now?
}

-(IBAction)onNavigationBack:(id)sender;
-(IBAction)onRefresh:(id)sender;
-(void)loadFeedURL:(NSString*)strurl;

@property (nonatomic, retain) NSArray *itemsToDisplay;

@end
