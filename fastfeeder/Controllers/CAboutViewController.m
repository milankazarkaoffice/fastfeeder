//
//  CAboutViewController.m
//  fastfeeder
//
//  Created by Milan Kazarka on 9/30/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import "CAboutViewController.h"
#import "FliteTTS.h"
#import "CT2S.h"

@interface CAboutViewController ()

@end

@implementation CAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    return self;
}

- (void)viewDidLoad
{
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    gesture.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:gesture];
    
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSString *tell = [NSString stringWithFormat:@"About FastFeeder screen loaded. Swipe downwards to go back to main screen. \
                      The FastFeeder app let's you listen to R S S news from various sources. It's controlled using gestures. Created by vysoko systems limited. Visit us at vysoko dot com"];
    
    CT2S *t2s = [CT2S instance];
    [t2s->fliteEngine stopTalking];
    [t2s->fliteEngine speakText:tell];
}

-(void)didSwipe:(UIGestureRecognizer *)gestureRecognizer
{
    NSLog(@"CPageViewController didSwipe");
    [self onDone:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(IBAction)onDone:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
