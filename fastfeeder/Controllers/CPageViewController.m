//
//  CPageViewController.m
//  fastfeeder
//
//  Created by Milan Kazarka on 8/9/13.
//  Copyright (c) 2013 Milan Kazarka. All rights reserved.
//

#import "CPageViewController.h"
#import "CPageCell.h"
#import "NSString+HTML.h"
#import "CT2S.h"
#import "FliteTTS.h"

@interface CPageViewController ()

@end

@implementation CPageViewController

@synthesize itemsToDisplay;

+(UITableViewCell*) createNewCustomCellFromNibNamed:(NSString*)name {
    NSArray* nibContents = [[NSBundle mainBundle]
                            loadNibNamed:name owner:self options:NULL];
    NSEnumerator *nibEnumerator = [nibContents objectEnumerator];
    UITableViewCell *customCell= nil;
    NSObject* nibItem = nil;
    while ( (nibItem = [nibEnumerator nextObject]) != nil) {
        if ( [nibItem isKindOfClass: (NSClassFromString(name))]) {
            customCell = (UITableViewCell*) nibItem;
            if ([customCell.reuseIdentifier isEqualToString: name]) {
                break; // we have a winner
            }
            
        }
    }
    return customCell;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    return self;
}

-(void)didSwipe:(UIGestureRecognizer *)gestureRecognizer
{
    NSLog(@"CPageViewController didSwipe");
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    NSLog(@"CPageViewController viewDidLoad");
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    gesture.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:gesture];
    
    [super viewDidLoad];
    
    mscrollView.delegate = self;
    mpages = [[NSMutableArray alloc] init];
    
    /**
    NSURL *feedURL = [NSURL URLWithString:@"http://feeds.reuters.com/Reuters/worldNews"];
	feedParser = [[MWFeedParser alloc] initWithFeedURL:feedURL];
	feedParser.delegate = self;
	feedParser.feedParseType = ParseTypeFull; // Parse feed info and all items
	feedParser.connectionType = ConnectionTypeAsynchronously;
    
    self.itemsToDisplay = [NSArray array];
    parsedItems = [[NSMutableArray alloc] init];
    
	[feedParser parse];
    */
    
    /**
    NSLog(@"scrollView size %fx%f",mscrollView.frame.size.width,mscrollView.frame.size.height);
    
    [mscrollView setContentSize:CGSizeMake(mscrollView.frame.size.width*3,mscrollView.frame.size.height)];
    int n;
    for(n = 0; n < 3; n++)
    {
        CPageCell *mpage = (CPageCell*)[CPageViewController createNewCustomCellFromNibNamed:@"CPageCell"];
        mpage.selectionStyle = UITableViewCellSelectionStyleNone;
        [mpage setFrame:CGRectMake(n*mscrollView.frame.size.width,0.0f,mscrollView.frame.size.width,mscrollView.frame.size.height)];
        
        //UIView *mpage = [[UIView alloc] initWithFrame:CGRectMake(n*mscrollView.frame.size.width,0.0f,mscrollView.frame.size.width,mscrollView.frame.size.height)];
        
        [mscrollView addSubview:mpage];
    }
    */
    
    NSString *tell = @"Loading feed items.";
    
    CT2S *t2s = [CT2S instance];
    [t2s->fliteEngine stopTalking];
    [t2s->fliteEngine speakText:tell];
}

-(void)loadURL:(NSString*)strurl
{
    if (!strurl)
        return;
    
    // @"http://feeds.reuters.com/Reuters/worldNews"
    NSURL *feedURL = [NSURL URLWithString:strurl];
	feedParser = [[MWFeedParser alloc] initWithFeedURL:feedURL];
	feedParser.delegate = self;
	feedParser.feedParseType = ParseTypeFull; // Parse feed info and all items
	feedParser.connectionType = ConnectionTypeAsynchronously;
    
    self.itemsToDisplay = [NSArray array];
    parsedItems = [[NSMutableArray alloc] init];
    
	[feedParser parse];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    [HUD show:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(IBAction)onNavigationBack:(id)sender
{
    CT2S *t2s = [CT2S instance];
    [t2s->fliteEngine stopTalking];
    [self.navigationController popViewControllerAnimated:YES];
}

//

- (void)updateTableWithParsedItems {
	self.itemsToDisplay = parsedItems;
	
    NSLog(@"itemsToDisplay %d",[itemsToDisplay count]);
    
    [mscrollView setContentSize:CGSizeMake(mscrollView.frame.size.width*[itemsToDisplay count],mscrollView.frame.size.height)];
    int n;
    for(n = 0; n < [itemsToDisplay count]; n++)
    {
        CPageCell *mpage = (CPageCell*)[CPageViewController createNewCustomCellFromNibNamed:@"CPageCell"];
        mpage->delegate = self;
        mpage.selectionStyle = UITableViewCellSelectionStyleNone;
        [mpage setFrame:CGRectMake(n*mscrollView.frame.size.width,0.0f,mscrollView.frame.size.width,mscrollView.frame.size.height)];
        
        MWFeedItem *item = [itemsToDisplay objectAtIndex:n];
        if (item)
        {
            NSString *itemTitle = item.title ? [item.title stringByConvertingHTMLToPlainText] : @"[No Title]";
            NSString *itemSummary = item.summary ? [item.summary stringByConvertingHTMLToPlainText] : @"[No Summary]";
            
            [mpage->mtitle setText:itemTitle];
            [mpage->msumary setText:itemSummary];
            [mpage->mpaginginfo setText:[NSString stringWithFormat:@"%d/%d",n+1,[itemsToDisplay count]]];
            
            if (item.link)
                mpage->murl = [[NSString alloc] initWithString:item.link];
            else
                mpage->murl = @"";
            [mpages addObject:mpage];
        }
        
        [mscrollView addSubview:mpage];
    }
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedItem:(MWFeedItem *)item {
	NSLog(@"Parsed Feed Item: “%@”", item.title);
	if (item) [parsedItems addObject:item];
}

- (void)feedParserDidFinish:(MWFeedParser *)parser {
	NSLog(@"Finished Parsing%@", (parser.stopped ? @" (Stopped)" : @""));
    [self updateTableWithParsedItems];
    
    if (HUD)
    {
        [HUD hide:YES];
    }
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedInfo:(MWFeedInfo *)info {
	NSLog(@"Parsed Feed Info: “%@”", info.title);
    [mfeedtitle setText:info.title];
    
    NSString *tell = [NSString stringWithFormat:@"%@ loaded. You are at item one. Swipe the screen left or right to select item. Tap to screen to hear the headline. Swipe upwards to go back to the main screen.",info.title];
    
    CT2S *t2s = [CT2S instance];
    [t2s->fliteEngine stopTalking];
    [t2s->fliteEngine speakText:tell];
}

-(void)onPageCellPlay:(id)cell
{
    CT2S *t2s = [CT2S instance];
    //[t2s->fliteEngine stopTalking];
    
    CPageCell *pc = cell;
    
    NSString *tell = @"";
    if ([pc->msumary.text length]>0)
    {
        //tell = [[NSString alloc] initWithFormat:@"%@",[pc->msumary.text stringByConvertingHTMLToPlainText]];
        tell = [[NSString alloc] initWithFormat:@"%@ ... ... ... ... ... ... ... ... %@",[pc->mtitle.text stringByConvertingHTMLToPlainText],[pc->msumary.text stringByConvertingHTMLToPlainText]];
    }
    else
    {
        tell = [[NSString alloc] initWithFormat:@"%@",[pc->mtitle.text stringByConvertingHTMLToPlainText]];
    }
    
    tell = [tell stringByReplacingOccurrencesOfString:@"|" withString:@""];
    tell = [tell stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    tell = [tell stringByReplacingOccurrencesOfString:@";" withString:@""];
    tell = [tell stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    tell = [tell stringByReplacingOccurrencesOfString:@"'" withString:@""];
    tell = [tell stringByReplacingOccurrencesOfString:@"US" withString:@" U S"];
    tell = [tell stringByReplacingOccurrencesOfString:@"USA" withString:@"U S A"];
    tell = [tell stringByReplacingOccurrencesOfString:@"UK" withString:@" U K"];
   
    [t2s->fliteEngine speakText:@"Loading item, please wait."];
    //[t2s->fliteEngine speakText:[pc->mtitle.text stringByConvertingHTMLToPlainText]];
    //if ([pc->msumary.text length]>0)
        [t2s->fliteEngine speakText:tell];
}

-(void)onPageOpenURL:(id)cell
{
    CPageCell *pc = cell;
    if ([pc->murl length]>0)
    {
        NSURL *url = [NSURL URLWithString:pc->murl];
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewDidScroll");
    
    int n;
    for(n = 0; n < [mpages count]; n++)
    {
        if (
            scrollView.contentOffset.x==(mscrollView.frame.size.width*n)
            )
        {
            MWFeedItem *item = [itemsToDisplay objectAtIndex:n];
            if (item)
            {
                CT2S *t2s = [CT2S instance];
                [t2s->fliteEngine stopTalking];
                [t2s->fliteEngine speakText:[NSString stringWithFormat:@"Item %d",n+1]];
            }
            break;
        }
    }
}


@end
